import numpy as np
from sklearn.utils.extmath import randomized_svd
import pickle
import csv
import time
import math
import scipy.sparse as sp
import matplotlib.pyplot as plt

class LSA():
    def __init__(self):
        self.language = 'kr'
        self.data_path = 'Processed_Data/'
        self.save_path = 'Trained_Data/'
        self.distinguish_folder = False
        self.idf_mult = 2
        self.raw_data = []
        self.NE = []
        self.OM = []
        self.TFIDF = []
        self.TFEnt = []
        self.corr = []
        self.noise = {'"', "'", '!', '.', ',', ';', ':', '`', '”', '“', ')', '(', '—', '’', '@', '%', '-', ' ', '월'}
        self.negative_set = {'유안타증권', '흥국증권', '한국투자증권', '미래에셋대우', '신한금융투', '한화투자증권',
                             '현대차투자증권', '하이투자증권', '리서치본부', '이베스트투자증권', '투자증권', '별도기준',
                             '하나금융투', '케이프투자증권', '기준자료', '억원', '리서치센터'}

    def read_raw_data(self, file_name):
        f = open(self.data_path + file_name, 'rb')
        dat = pickle.load(f)
        self.raw_data = dat
        f.close()

    def read_data(self, file_name):
        f = open(file_name, 'rb')
        dat = pickle.load(f)
        f.close()
        return dat

    def save(self, file, file_name='No_Title'):
        pickle.dump(file, open(self.save_path + file_name, 'wb'), protocol=4)

    def extract_NE(self):
        if len(self.raw_data) == 0:
            raise Exception('Data not loaded. Please load data with "read_raw_data" function')
        NE_raw = list(set([words for sents in self.raw_data for words in sents]))
        NE_clean = []
        for word in NE_raw:
            temp = ''
            for charact in word:
                if charact.isalpha():
                    temp += charact
            if len(temp) > 1:
                NE_clean.append(temp)

        NE_clean = set(NE_clean)
        for negative_word in self.negative_set:
            if negative_word in NE_clean:
                NE_clean.remove(negative_word)
        return list(NE_clean)

    def view_word_sum_dist(self):
        start = time.time()
        if len(self.NE) == 0:
            self.NE = self.extract_NE()
            print('Named entity list created')
        word_count = np.zeros((len(self.NE)))
        process_index_set = set([math.floor(i * len(self.raw_data) / 100) for i in range(100)])
        tot_ind = 0
        cur_ind = 0
        for doc in self.raw_data:
            if cur_ind in process_index_set:
                print('word sum count : ',tot_ind, '% Done')
                tot_ind += 1
            for word in doc:
                try:
                    index = self.NE.index(word)
                except:
                    continue
                word_count[index] += 1
            cur_ind += 1
        plt.hist(word_count, bins='auto')
        plt.show()
        end = time.time()
        print('Time: ', end - start)

    def calcOM(self, OM_reduction_threshold = 5):
        # This function calculates Occurence matrix
        # This function also reduces NE according to the given minimum term frequency
        # Class of NE and OM becomes numpy array
        """
        OM = np.zeros((len(self.NE), len(self.raw_data)))
        for NE_Index in range(len(self.NE)):
            for doc_Index in range(len(self.raw_data)):
                OM[NE_Index][doc_Index] = self.raw_data[doc_Index].count(self.NE[NE_Index])
        if OM_reduction_threshold == 0:
            self.OM = OM
        else:
            rowSum = np.sum(OM, 1, keepdims=True)
            self.NE = np.array(self.NE)
            idx = np.where(rowSum >= OM_reduction_threshold)[0]
            self.NE = np.transpose(self.NE[idx])
            self.OM = OM[idx, :]
            """
        start = time.time()
        print('Constructing Occurence Matrix')
        process_index_set = set([math.floor(i * len(self.NE) / 100) for i in range(100)])
        keep_index = []
        tot_ind = 0
        for NE_Index in range(len(self.NE)):
            if NE_Index in process_index_set:
                print('Counting matrix construction: ',tot_ind, '% Done')
                tot_ind += 1

            row =[]
            column = []
            data = []
            for doc_Index in range(len(self.raw_data)):
                temp = self.raw_data[doc_Index].count(self.NE[NE_Index])
                if temp > 0:
                    row.append(0)
                    column.append(doc_Index)
                    data.append(temp)
            if sum(data) > OM_reduction_threshold:
                keep_index.append(NE_Index)
                if len(keep_index) == 1:
                    OM = sp.csr_matrix((data, (row, column)), shape=(1, len(self.raw_data)))
                else:
                    OM = sp.vstack([OM, sp.csr_matrix((data, (row, column)), shape=(1, len(self.raw_data)))])
        self.NE = np.array(self.NE)
        self.NE = self.NE[keep_index]
        self.OM = OM
        end = time.time()
        print('OM construction time: ', end-start)

    def calcTFIDF(self, OM_reduction_threshold = 5):
        if len(self.NE) == 0:
            self.NE = self.extract_NE()
            print('Named entity list created')
        if not isinstance(self.OM, sp.csr.csr_matrix):
            self.calcOM(OM_reduction_threshold)
            print('Occurence matrix calculated')
        tfMat = calcTF(self.OM)
        idf = calcIDF(self.OM)
        idf = np.power(idf, self.idf_mult) # ------ Caution
        self.TF = tfMat
        self.IDF = idf
        tfidfMat = np.multiply(tfMat, idf)
        self.TFIDF = tfidfMat

    def calcTFEnt(self, OM_reduction_threshold = 5):
        if len(self.NE) == 0:
            self.NE = self.extract_NE()
            print('Named entity list created')
        if len(self.OM) == 0:
            self.calcOM(OM_reduction_threshold)
            print('Occurence matrix calculated')

        gf = np.sum(self.OM, 1, keepdims=True)
        p = np.zeros(self.OM.shape)
        for word_ind in range(self.OM.shape[0]):
            p[word_ind][:] = self.OM[word_ind][:]/(gf[word_ind]*np.ones(self.OM.shape[1]))
        g = np.ones(self.OM.shape[0])
        for i in range(self.OM.shape[0]):
            for j in range(self.OM.shape[1]):
                if p[i][j] == 0:
                    pass
                else:
                    g[i] += p[i][j]*np.log(p[i][j])/np.log(self.OM.shape[1])

        self.save_as_csv(np.transpose(np.array([g])), 'Ent')  # Remove This Line
        tfMat = calcTF(self.OM)
        self.TFEnt = np.log(tfMat + 1)
        for i in range(self.OM.shape[0]):
            self.TFEnt[i][:] = g[i]*self.TFEnt[i][:]
        self.save_as_csv(tfMat, 'TF')  # Remove This Line
        self.save_as_csv(self.TFEnt, 'TFEnt')  # Remove This Line

    def calcCorrMat(self, OM_reduction_threshold = 5, method = 0, svd_reduc_num = 0):
        # Method: 0: TD-IDF
        #         1: Entropy
        # svd_reduc_num: If 0 it does not perform SVD. If integer > 0, it performs SVD and selects the largest
        #                svd_reduc_num many singular values and its corresponding eigenvectors to approximate corr
        start = time.time()
        if method == 0 or method == 'TFIDF' or method == 'tfidf':
            if len(self.TFIDF) == 0:
                self.calcTFIDF(OM_reduction_threshold)
                print('TF-IDF Matrix calculated. OM/NE Reduction TF threshold value is : ', OM_reduction_threshold)
                print("Dimension of TF-IDF Matrix is :" + str(self.TFIDF.shape))
            if svd_reduc_num > 0 and isinstance(svd_reduc_num, int):
                U, Sigma, VT = randomized_svd(self.TFIDF, n_components=svd_reduc_num, n_iter=7, random_state=None)
                self.TFIDF = np.matmul(U, np.diag(Sigma))
                print('Dimension Reduced. TFIDF shape :', self.TFIDF.shape)

            self.corr = np.matmul(self.TFIDF, np.transpose(self.TFIDF))
            print('Correlation Matrix calculated. Shape is :', self.corr.shape)

    def calc_reduced_TFIDF(self, OM_reduction_threshold=5, method=0, svd_reduc_num=0):
        # Method: 0: TD-IDF
        #         1: Entropy
        # svd_reduc_num: If 0 it does not perform SVD. If integer > 0, it performs SVD and selects the largest
        #                svd_reduc_num many singular values and its corresponding eigenvectors to approximate corr
        start = time.time()
        if method == 0 or method == 'TFIDF' or method == 'tfidf':
            if len(self.TFIDF) == 0:
                self.calcTFIDF(OM_reduction_threshold)
                print('TF-IDF Matrix calculated. OM/NE Reduction TF threshold value is : ', OM_reduction_threshold)
                print("Dimension of TF-IDF Matrix is :" + str(self.TFIDF.shape))
            if svd_reduc_num > 0 and isinstance(svd_reduc_num, int):
                U, Sigma, VT = randomized_svd(self.TFIDF, n_components=svd_reduc_num, n_iter=7, random_state=None)
                self.TFIDF = np.matmul(U, np.diag(Sigma))
                print('Dimension Reduced. TFIDF shape :', self.TFIDF.shape)
        elif method == 1 or method == 'entropy' or method == 'Entropy':
            if len(self.TFEnt) == 0:
                self.calcTFEnt(OM_reduction_threshold)
                print('TF-Entropy Matrix calculated. OM/NE Reduction TF threshold value is : ', OM_reduction_threshold)
                print("Dimension of TF-Entropy Matrix is :" + str(self.TFEnt.shape))
            if svd_reduc_num > 0 and isinstance(svd_reduc_num, int):
                U, Sigma, VT = randomized_svd(self.TFEnt, n_components=svd_reduc_num, n_iter=7, random_state=None)
                self.TFEnt = np.matmul(U, np.diag(Sigma))
                print('Dimension Reduced. TF-Entropy shape :', self.TFEnt.shape)
        else:
            raise Exception('Invalid method. Only 0: TDIDF, 1: Entropy scheme available')
        end = time.time()
        print('LSA Processing time: ', end - start)





    def find_NE_ind(self, name):
        if isinstance(self.NE, np.ndarray):
            try:
                idx = np.where(self.NE ==name)[0][0]
            except IndexError:
                print('The given string" ', name, '" is not in the NE list')
                return []
        elif isinstance(self.NE, list):
            try:
                idx = self.NE.index(name)
            except ValueError:
                print('The given string "', name,'" is not in the NE list')
                return []
        return int(idx)

    def find_connections(self, name_or_ID, threshold = 1):
        if not isinstance(self.NE, np.ndarray):
            self.NE = np.array(self.NE)

        if (isinstance(self.corr, np.ndarray) and self.corr.shape[0] != 0) or  (isinstance(self.corr, list) and len(self.corr) != 0):
            if isinstance(name_or_ID, str):
                try:
                    idx = np.where(self.NE ==name_or_ID)[0][0]
                except ValueError:
                    print('The given string is not in the NE list')
                sorted_connections = filter_top_connections(self.corr, idx, threshold)
                if len(sorted_connections) == 0:
                    print('No match for given threshold: ', name_or_ID)
                    print('')
                    return [], []
                connections_str = self.NE[sorted_connections]
                print('Topic : ', name_or_ID)
                print('Connections :', connections_str)
                print('')
                correlation = self.corr[idx][sorted_connections]
            elif isinstance(name_or_ID, int):
                sorted_connections = filter_top_connections(self.corr, name_or_ID, threshold)
                if len(sorted_connections) == 0:
                    print('No match for given threshold: ', self.NE[name_or_ID])
                    print('')
                    return [], []
                connections_str = self.NE[sorted_connections]
                print('Topic : ', self.NE[name_or_ID])
                print('Connections :', connections_str)
                print('')
                correlation = self.corr[name_or_ID][sorted_connections]
            elif isinstance(name_or_ID, list) and isinstance(name_or_ID[0], str):
                connections_str = []
                correlation = []
                for i in range(len(name_or_ID)):
                    try:
                        idx = np.where(self.NE == name_or_ID[i])[0][0]
                    except ValueError:
                        print('The string', name_or_ID[i] ,'is not in the NE list')
                        print('')
                        connections_str.append([])
                        correlation.append([])
                        continue
                    sorted_connections = filter_top_connections(self.corr, idx, threshold)
                    connections_str.append(self.NE[sorted_connections])
                    print('Topic : ', name_or_ID[i])
                    print('Connections :', connections_str[i])
                    print('')
                    correlation.append(self.corr[idx][sorted_connections])
            elif isinstance(name_or_ID, list) and isinstance(name_or_ID[0], int):
                connections_str = []
                correlation = []
                for i in range(len(name_or_ID)):
                    sorted_connections = filter_top_connections(self.corr, name_or_ID[i], threshold)
                    connections_str.append(self.NE[sorted_connections])
                    print('Topic : ', self.NE[name_or_ID[i]])
                    print('Connections :', connections_str[i])
                    print('')
                    correlation.append(self.corr[name_or_ID[i]][sorted_connections])
            else:
                raise Exception('Invalid input. name_or_ID must be str, int or list of str, int')
        # TFIDF Base -----------------------------------------------------------------------------------
        else:
            if isinstance(name_or_ID, str):
                try:
                    idx = np.where(self.NE ==name_or_ID)[0][0]
                except ValueError:
                    print('The given string is not in the NE list')
                sorted_connections, correlation = filter_top_connections_tfidf(self.TFIDF, idx, threshold)
                if len(sorted_connections) == 0:
                    print('No match for given threshold: ', name_or_ID)
                    print('')
                    return [], []
                connections_str = self.NE[sorted_connections]
                print('Topic : ', name_or_ID)
                print('Connections :', connections_str)
                print('')
            elif isinstance(name_or_ID, int):
                sorted_connections, correlation = filter_top_connections_tfidf(self.TFIDF, name_or_ID, threshold)
                if len(sorted_connections) == 0:
                    print('No match for given threshold: ', self.NE[name_or_ID])
                    print('')
                    return [], []
                connections_str = self.NE[sorted_connections]
                print('Topic : ', self.NE[name_or_ID])
                print('Connections :', connections_str)
                print('')
            elif isinstance(name_or_ID, list) and isinstance(name_or_ID[0], str):
                connections_str = []
                correlation = []
                for i in range(len(name_or_ID)):
                    try:
                        temp = np.where(self.NE == name_or_ID[i])
                        idx = np.where(self.NE == name_or_ID[i])[0][0]
                    except IndexError:
                        print('The string', name_or_ID[i] ,'is not in the NE list')
                        print('')
                        connections_str.append([])
                        correlation.append([])
                        continue
                    sorted_connections, sorted_corr = filter_top_connections_tfidf(self.TFIDF, idx, threshold)
                    connections_str.append(self.NE[sorted_connections])
                    print('Topic : ', name_or_ID[i])
                    print('Connections :', connections_str[i])
                    print('')
                    correlation.append(sorted_corr)
            elif isinstance(name_or_ID, list) and isinstance(name_or_ID[0], int):
                connections_str = []
                correlation = []
                for i in range(len(name_or_ID)):
                    sorted_connections, sorted_corr = filter_top_connections_tfidf(self.TFIDF, name_or_ID[i], threshold)
                    connections_str.append(self.NE[sorted_connections])
                    print('Topic : ', self.NE[name_or_ID[i]])
                    print('Connections :', connections_str[i])
                    print('')
                    correlation.append(sorted_corr)
            else:
                raise Exception('Invalid input. name_or_ID must be str, int or list of str, int')
        return connections_str, correlation

    def save_as_csv(self, file, filename):
        f = open(filename+'.csv', 'w', encoding='euc_kr', newline='')
        wr = csv.writer(f)
        for i in range(file.shape[0]):

            temp = []
            temp.append(self.NE[i])
            for j in range(len(file[i][:])):
                temp.append(file[i][j])
            wr.writerow(temp)
        f.close()

    def save_NE_as_csv(self, file, filename):
        f = open(filename+'.csv', 'w', newline='')
        wr = csv.writer(f)
        for i in range(len(file)):
            try:
                wr.writerow([file[i]])
            except UnicodeEncodeError:
                continue
        f.close()

def filter_top_connections(cormat, idx, threshold):
    if threshold == 'all':
        return np.argsort(cormat[idx][:])[::-1]
    elif isinstance(threshold, str):
        threshold = int(threshold)
        sorted = np.argsort(cormat[idx][:])[::-1]
        return sorted[0:threshold]
    elif isinstance(threshold, float) or isinstance(threshold, int):
        qualified_index = np.where(cormat[idx][:]>threshold)[0]
        if len(qualified_index) == 0:
            return []
        sorted = cormat[idx][qualified_index]
        sortofsort = np.argsort(sorted)[::-1]
        return qualified_index[sortofsort]

def filter_top_connections_tfidf(idf, idx, threshold):
    word_corr_row =np.array(np.transpose(np.matmul(idf, np.transpose(idf[idx, :]))))
    word_corr_row = np.squeeze(word_corr_row)
    if threshold == 'all':
        return np.argsort(word_corr_row)[::-1], word_corr_row[np.argsort(word_corr_row)[::-1]]
    elif isinstance(threshold, str):
        threshold = int(threshold)
        sorted = np.argsort(word_corr_row)[::-1]
        a = sorted[0:threshold]
        b = word_corr_row[sorted[0:threshold]]
        return sorted[0:threshold], word_corr_row[sorted[0:threshold]]
    elif isinstance(threshold, float) or isinstance(threshold, int):
        qualified_index = np.where(word_corr_row > threshold)[0]
        if len(qualified_index) == 0:
            return [], []
        sorted = word_corr_row[qualified_index]
        sortofsort = np.argsort(sorted)[::-1]
        return qualified_index[sortofsort], word_corr_row[qualified_index[sortofsort]]

# Calculate Term Frequency (Normalization)
def calcTF(countMat):
    colSum = countMat.sum(axis=1)
    tfMat = countMat / (colSum + 0.0001)  # colSum[np.newaxis,:]
    return tfMat

# Calculate Inverse Document Frequency
def calcIDF(countMat):
    termNum, docNum = countMat.shape
    rowsum = np.zeros((termNum,1))
    indicator = countMat.nonzero()
    for i in indicator[0]:
        rowsum[i, 0] += 1
    del indicator
    idf = np.log(docNum / (1 + rowsum))
    return idf









