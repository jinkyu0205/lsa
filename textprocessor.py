# -*- coding: utf-8 -*-

import nltk
import os
import pickle
import numpy as np
from nltk import word_tokenize
from nltk.corpus import stopwords
import math
import time
import konlpy
import re
import pandas as pd


class processor():
    def __init__(self):
        self.language = 'kr'
        self.data_path = ["Raw_Data/fn_guide/"]
        self.save_path = 'Processed_Data/'
        self.files = []
        self.filter_words = ['본 자료', '본 조사분석자료에','변경될 수 있습니다', '이 보고서는', '바랍니다', '당사는'
                            , '수 없습니다', '법적 책임소재를', '법적 책임소재의', '동의 없이', '작성되었음을', '제공된 사실이 없습니다'
                             , '이해 관계가 없습니다', '자료:', '주:'
                             ] #If you find this phrase in a line, you exclude that line from analysis.


    def file_list(self, csv_file = '', condition_list = []):
        # This function returns the full list of files in the data path
        # If csv_file is an empty string, this returns all the files in the self.data_path folders
        if len(self.files) > 0:
            print('Warning: the file list already exist')

        if len(csv_file) == 0:
            if isinstance(self.data_path, str):
                if self.data_path[-1] != '/':
                    self.data_path += '/'
                self.data_path = [self.data_path]
            print('Folder Directory: ')
            for q in range(len(self.data_path)):
                print(q, self.data_path[q])
            temp = []
            for path in self.data_path:
                files = (os.listdir(path))
                temp.append([(path + words) for words in files])
            self.files = [files for folder in temp for files in folder]
            del temp
        elif len(csv_file) > 0:
            if isinstance(self.data_path, str):
                if self.data_path[-1] != '/':
                    self.data_path += '/'
                self.data_path = [self.data_path]
            elif isinstance(self.data_path, list) and len(self.data_path) == 1 and self.data_path[0][-1] != '/':
                self.data_path = [self.data_path[0]+'/']

            a = self.data_path[0] + csv_file
            df = pd.read_csv(self.data_path[0] + csv_file, engine='python')
            fold_dir = df['dir_txt']
            file_dir = df['fn_txt']

            if len(condition_list) > 0:
                filtered_index = list(range(0, df.shape[0]))
                local_scope_dict = {'index_filter': []}
                for condition in condition_list:
                    condition = 'local_scope_dict["index_filter"] = ' + condition
                    exec(condition)
                    filtered_index = [val for val in filtered_index if (val in set(local_scope_dict['index_filter']))]

            fold_dir = fold_dir[filtered_index]
            file_dir = file_dir[filtered_index]
            self.files = [self.data_path[0] + fold_dir.iloc[i] + '/' + file_dir.iloc[i] for i in
                          range(len(fold_dir))]
            if len(self.files) > 0:
                print('File list created. Size: ', len(self.files))
            else:
                raise Exception('No data matches your condition')


    def txt_english_translator(self, dict_file = []):
        Tot_file_num = len(self.files)
        index_set = set([math.floor(i*Tot_file_num/100) for i in range(100)])
        Total_index = 0
        skip_number = 0
        if len(dict_file) == 0:
            raise Exception('Dictionary must be given as a dictionary or path to the dict file')
        elif isinstance(dict_file, str):
            f = open(dict_file, 'rb')
            dictionary = pickle.load(f)
            f.close()
            print('Translation dictionary loaded')
        elif isinstance(dict_file, dict):
            dictionary = dict_file
        else:
            raise Exception('Dictionary must be given as a dictionary or path to the dict file')

        for txt_ind in self.files:

            Total_index += 1

            if Total_index in index_set:
                print('Raw data English to Korea translation: ', math.floor((Total_index + 1)/Tot_file_num*100), '% Done')
            try:
                f = open(txt_ind, 'r',  encoding='UTF-8')
            except FileNotFoundError:
                skip_number += 1
                continue
            lines = f.readlines()
            f.close()
            for ind, line in enumerate(lines):
                for dict_word_en in dictionary:
                    line = line.replace(dict_word_en, dictionary[dict_word_en])
                lines[ind] = line
            # print(lines[-1])
            f = open(txt_ind, 'w', encoding='UTF-8')
            f.writelines(lines)
            f.close()
        print('Done. Skipped ', skip_number, ' Documents')

    def txt_non_korean_remover(self):
        hangul = re.compile('[^ ㄱ-ㅣ가-힣]+')
        Tot_file_num = len(self.files)
        index_set = set([math.floor(i*Tot_file_num/100) for i in range(100)])
        Total_index = 0
        skip_number = 0
        if Tot_file_num == 0:
            raise Exception('Error: File set not determined')
        for txt_ind in self.files:

            Total_index += 1

            if Total_index in index_set:
                print('Extracting only Korean from Original Text: ', math.floor((Total_index + 1)/Tot_file_num*100), '% Done')
            try:
                f = open(txt_ind, 'r',  encoding='UTF-8')
            except FileNotFoundError:
                skip_number += 1
                continue
            lines = f.readlines()
            f.close()
            cleared_doc = []
            for line_ind, line in enumerate(lines):
                cleared_line = hangul.sub('', line)
                if len(cleared_line.replace(" ", ""))>0:
                    cleared_doc.append(cleared_line+'\n')
            f = open(txt_ind, 'w', encoding='UTF-8')
            f.writelines(cleared_doc)
            f.close()
        print('Done. Skipped ', skip_number, ' Documents')

    def NYT_Processor(self, all_files):
        if self.language == 'en':
            stopWords = set(stopwords.words('english'))
            punctuations = {'"', "'", '!', '.', ',', ';', ':', '`', '”', '“', ')', '(', '—', '’', '@'}
            processed_doc = []
            only_doc = []
            for doc_ind in all_files:
                f = open(self.data_path + doc_ind, 'rt', encoding='UTF8')
                lines = f.readlines()
                manually_processed = [line[0:-1] for ind, line in enumerate(lines) if (len(
                    line) > 2 and line != 'Advertisement\n' and line != 'This is your last free article.\n' and ind not in [
                                                                                           2, 3, 4,
                                                                                           5])]  # This is not a smart way to do but it is easy and fast so implemented this way for now.
                doc_output = []
                doc_output_only_doc = []
                for line in manually_processed:
                    line_output = []
                    text = [word for word in word_tokenize(line) if
                            ((word not in stopWords) and (word not in punctuations))]
                    tagged = nltk.pos_tag(text)
                    NER_Recognition = nltk.ne_chunk(tagged, binary=True)
                    for word in NER_Recognition:
                        if type(word) == nltk.tree.Tree:
                            NE = ''
                            for i in range(len(word)):
                                NE += word[i][0]
                                NE += ' '
                            line_output.append(NE[0:-1])
                            doc_output_only_doc.append(NE[0:-1])
                        elif word[1][0:2] == 'NN':
                            line_output.append(word[0])
                            doc_output_only_doc.append(word[0])
                    doc_output.append(line_output)
                processed_doc.append(doc_output)
                only_doc.append(doc_output_only_doc)
            return processed_doc, only_doc


    def easy_process(self, tagger = 'Mecab'):

        start = time.time()
        print('Korean Documents Processor')
        if tagger == 'Mecab':
            konlp_tagger = konlpy.tag.Mecab()
            print('Tagger: Mecab')
        elif tagger == 'Kkma':
            konlp_tagger = konlpy.tag.Kkma()
            print('Tagger: Kkma')
        elif tagger == 'Twitter':
            konlp_tagger = konlpy.tag.Twitter()
            print('Tagger: Twitter')
        elif tagger == 'Hannanum':
            konlp_tagger = konlpy.tag.Hannanum()
            print('Tagger: Hannanum')
        else:
            raise Exception(tagger, ' is not a tagger defined in konlpy')



        output = []
        Tot_file_num = len(self.files)
        index_set = set([math.floor(i*Tot_file_num/100) for i in range(100)])
        Total_index = 0
        for doc_ind in self.files:
            Total_index += 1

            if Total_index in index_set:
                print('Text Processing ', math.floor((Total_index + 1)/Tot_file_num*100), '% Done')

            try:
                f = open(doc_ind, 'rt', encoding='UTF8')
            except FileNotFoundError:
                print('Doc:', doc_ind, "not found")
                continue
            lines = f.readlines()
            f.close()
            processed_lines = line_detection(lines)
            # Remove unnecessary lines that contain a word phrase from self.filter_words
            line_removal_index = []
            for indx, lines in enumerate(processed_lines):
                filter_ind = [lines.find(filter_word) for filter_word in self.filter_words]
                filter_ind = [positive for positive in filter_ind if positive != -1]
                if len(filter_ind) != 0:
                   line_removal_index.append(indx)
                else:
                    temp_line = [tword for tword in lines.split() if len(tword) > 15]
                    temp_line2 = [tword for tword in lines.split() if len(tword) < 15]
                    if len(temp_line2) == 0:
                        line_removal_index.append(indx)
                    elif len(temp_line) != 0:
                        line_removal_index.append(indx)
                        # processed_lines[indx] = ' '.join(temp_line)

            if len(line_removal_index)>0:
                line_removal_index = line_removal_index[::-1]
                for i in range(len(line_removal_index)):
                   del processed_lines[line_removal_index[i]]

            if len(processed_lines) > 200:
                word = []
                for i in range(len(processed_lines)//200):
                    if i == len(processed_lines) -1:
                        doc = ''.join(processed_lines[i * 200:])
                        word.append(konlp_tagger.nouns(doc))
                    doc = ''. join(processed_lines[i*200:(i+1)*200])
                    word.append(konlp_tagger.nouns(doc))
                words = [element for partition in word for element in partition if len(element) > 1]
            else:
                doc = ''.join(processed_lines)
                words = konlp_tagger.nouns(doc)
                words = [word for word in words if len(word)>1]

            output.append(words)
        end = time.time()
        print('Data Processing Computation Time :', end - start, ' seconds')
        return output

    def save(self, file, file_name='No_Title'):
        fi = open(self.save_path + file_name, 'wb')
        pickle.dump(file, fi)
        fi.close()

def line_detection(lines):
    lin_num = len(lines)
    clean_lines = []
    line_index = 0

    hangul = re.compile('[^ ㄱ-ㅣ가-힣]+')
    while line_index < lin_num:
        if len(lines[line_index]) >= 15:
            candidate = hangul.sub('', lines[line_index])
            if len(candidate) > 10:
                cleaned = [char for char in lines[line_index] if (char.isalpha() or char == ' ')]
                clean_lines.append(''.join(cleaned))
        line_index += 1

    return clean_lines

def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True