from nltk import word_tokenize
from nltk.corpus import stopwords
from SPARQLWrapper import SPARQLWrapper, JSON
from selenium import webdriver
from bs4 import BeautifulSoup
import nltk
import os
import pickle
import math
import time
import konlpy
import numpy.random as rd
import re
import pandas as pd
import requests
import LSA_ybl



class processor():
    def __init__(self):
        self.language = 'kr'
        self.data_path = ["Raw_Data/fn_guide/"]
        self.save_path = 'Processed_Data/'
        self.files = []
        self.filter_words = ['본 자료', '본 조사분석자료에','변경될 수 있습니다', '이 보고서는', '바랍니다', '당사는'
                            , '수 없습니다', '법적 책임소재를', '법적 책임소재의', '동의 없이', '작성되었음을', '제공된 사실이 없습니다'
                             , '이해 관계가 없습니다', '자료:', '주:'
                             ] #If you find this phrase in a line, you exclude that line from analysis.


    def file_list(self, csv_file = '', condition_list = []):
        # This function returns the full list of files in the data path
        # If csv_file is an empty string, this returns all the files in the self.data_path folders
        if len(self.files) > 0:
            print('Warning: the file list already exist')

        if len(csv_file) == 0:
            if isinstance(self.data_path, str):
                if self.data_path[-1] != '/':
                    self.data_path += '/'
                self.data_path = [self.data_path]
            print('Folder Directory: ')
            for q in range(len(self.data_path)):
                print(q, self.data_path[q])
            temp = []
            for path in self.data_path:
                files = (os.listdir(path))
                temp.append([(path + words) for words in files])
            self.files = [files for folder in temp for files in folder]
            del temp
        elif len(csv_file) > 0:
            if isinstance(self.data_path, str):
                if self.data_path[-1] != '/':
                    self.data_path += '/'
                self.data_path = [self.data_path]
            elif isinstance(self.data_path, list) and len(self.data_path) == 1 and self.data_path[0][-1] != '/':
                self.data_path = [self.data_path[0]+'/']

            a = self.data_path[0] + csv_file
            df = pd.read_csv(self.data_path[0] + csv_file, engine='python')
            fold_dir = df['dir_txt']
            file_dir = df['fn_txt']

            if len(condition_list) > 0:
                filtered_index = list(range(0, df.shape[0]))
                local_scope_dict = {'index_filter': []}
                for condition in condition_list:
                    condition = 'local_scope_dict["index_filter"] = ' + condition
                    exec(condition)
                    filtered_index = [val for val in filtered_index if (val in set(local_scope_dict['index_filter']))]

            fold_dir = fold_dir[filtered_index]
            file_dir = file_dir[filtered_index]
            self.files = [self.data_path[0] + fold_dir.iloc[i] + '/' + file_dir.iloc[i] for i in
                          range(len(fold_dir))]
            if len(self.files) > 0:
                print('File list created. Size: ', len(self.files))
            else:
                raise Exception('No data matches your condition')


    def txt_english_translator(self, dict_file = []):
        Tot_file_num = len(self.files)
        index_set = set([math.floor(i*Tot_file_num/100) for i in range(100)])
        Total_index = 0
        skip_number = 0
        if len(dict_file) == 0:
            raise Exception('Dictionary must be given as a dictionary or path to the dict file')
        elif isinstance(dict_file, str):
            f = open(dict_file, 'rb')
            dictionary = pickle.load(f)
            f.close()
            print('Translation dictionary loaded')
        elif isinstance(dict_file, dict):
            dictionary = dict_file
        else:
            raise Exception('Dictionary must be given as a dictionary or path to the dict file')

        for txt_ind in self.files:

            Total_index += 1

            if Total_index in index_set:
                print('Raw data English to Korea translation: ', math.floor((Total_index + 1)/Tot_file_num*100), '% Done')
            try:
                f = open(txt_ind, 'r',  encoding='UTF-8')
            except FileNotFoundError:
                skip_number += 1
                continue
            lines = f.readlines()
            f.close()
            for ind, line in enumerate(lines):
                for dict_word_en in dictionary:
                    line = line.replace(dict_word_en, dictionary[dict_word_en])
                lines[ind] = line
            # print(lines[-1])
            f = open(txt_ind, 'w', encoding='UTF-8')
            f.writelines(lines)
            f.close()
        print('Done. Skipped ', skip_number, ' Documents')

    def txt_non_korean_remover(self):
        hangul = re.compile('[^ ㄱ-ㅣ가-힣]+')
        Tot_file_num = len(self.files)
        index_set = set([math.floor(i*Tot_file_num/100) for i in range(100)])
        Total_index = 0
        skip_number = 0
        if Tot_file_num == 0:
            raise Exception('Error: File set not determined')
        for txt_ind in self.files:

            Total_index += 1

            if Total_index in index_set:
                print('Extracting only Korean from Original Text: ', math.floor((Total_index + 1)/Tot_file_num*100), '% Done')
            try:
                f = open(txt_ind, 'r',  encoding='UTF-8')
            except FileNotFoundError:
                skip_number += 1
                continue
            lines = f.readlines()
            f.close()
            cleared_doc = []
            for line_ind, line in enumerate(lines):
                cleared_line = hangul.sub('', line)
                if len(cleared_line.replace(" ", ""))>0:
                    cleared_doc.append(cleared_line+'\n')
            f = open(txt_ind, 'w', encoding='UTF-8')
            f.writelines(cleared_doc)
            f.close()
        print('Done. Skipped ', skip_number, ' Documents')

    def NYT_Processor(self, all_files):
        if self.language == 'en':
            stopWords = set(stopwords.words('english'))
            punctuations = {'"', "'", '!', '.', ',', ';', ':', '`', '”', '“', ')', '(', '—', '’', '@'}
            processed_doc = []
            only_doc = []
            for doc_ind in all_files:
                f = open(self.data_path + doc_ind, 'rt', encoding='UTF8')
                lines = f.readlines()
                manually_processed = [line[0:-1] for ind, line in enumerate(lines) if (len(
                    line) > 2 and line != 'Advertisement\n' and line != 'This is your last free article.\n' and ind not in [
                                                                                           2, 3, 4,
                                                                                           5])]  # This is not a smart way to do but it is easy and fast so implemented this way for now.
                doc_output = []
                doc_output_only_doc = []
                for line in manually_processed:
                    line_output = []
                    text = [word for word in word_tokenize(line) if
                            ((word not in stopWords) and (word not in punctuations))]
                    tagged = nltk.pos_tag(text)
                    NER_Recognition = nltk.ne_chunk(tagged, binary=True)
                    for word in NER_Recognition:
                        if type(word) == nltk.tree.Tree:
                            NE = ''
                            for i in range(len(word)):
                                NE += word[i][0]
                                NE += ' '
                            line_output.append(NE[0:-1])
                            doc_output_only_doc.append(NE[0:-1])
                        elif word[1][0:2] == 'NN':
                            line_output.append(word[0])
                            doc_output_only_doc.append(word[0])
                    doc_output.append(line_output)
                processed_doc.append(doc_output)
                only_doc.append(doc_output_only_doc)
            return processed_doc, only_doc


    def easy_process(self, tagger = 'Mecab'):

        start = time.time()
        print('Korean Documents Processor')
        if tagger == 'Mecab':
            konlp_tagger = konlpy.tag.Mecab()
            print('Tagger: Mecab')
        elif tagger == 'Kkma':
            konlp_tagger = konlpy.tag.Kkma()
            print('Tagger: Kkma')
        elif tagger == 'Twitter':
            konlp_tagger = konlpy.tag.Twitter()
            print('Tagger: Twitter')
        elif tagger == 'Hannanum':
            konlp_tagger = konlpy.tag.Hannanum()
            print('Tagger: Hannanum')
        else:
            raise Exception(tagger, ' is not a tagger defined in konlpy')



        output = []
        Tot_file_num = len(self.files)
        index_set = set([math.floor(i*Tot_file_num/100) for i in range(100)])
        Total_index = 0
        for doc_ind in self.files:
            Total_index += 1

            if Total_index in index_set:
                print('Text Processing ', math.floor((Total_index + 1)/Tot_file_num*100), '% Done')

            try:
                f = open(doc_ind, 'rt', encoding='UTF8')
            except FileNotFoundError:
                print('Doc:', doc_ind, "not found")
                continue
            lines = f.readlines()
            f.close()
            processed_lines = self.line_detection(lines)
            # Remove unnecessary lines that contain a word phrase from self.filter_words
            line_removal_index = []
            for indx, lines in enumerate(processed_lines):
                filter_ind = [lines.find(filter_word) for filter_word in self.filter_words]
                filter_ind = [positive for positive in filter_ind if positive != -1]
                if len(filter_ind) != 0:
                   line_removal_index.append(indx)
                else:
                    temp_line = [tword for tword in lines.split() if len(tword) > 15]
                    temp_line2 = [tword for tword in lines.split() if len(tword) < 15]
                    if len(temp_line2) == 0:
                        line_removal_index.append(indx)
                    elif len(temp_line) != 0:
                        line_removal_index.append(indx)
                        # processed_lines[indx] = ' '.join(temp_line)

            if len(line_removal_index)>0:
                line_removal_index = line_removal_index[::-1]
                for i in range(len(line_removal_index)):
                   del processed_lines[line_removal_index[i]]

            if len(processed_lines) > 200:
                word = []
                for i in range(len(processed_lines)//200):
                    if i == len(processed_lines) -1:
                        doc = ''.join(processed_lines[i * 200:])
                        word.append(konlp_tagger.nouns(doc))
                    doc = ''. join(processed_lines[i*200:(i+1)*200])
                    word.append(konlp_tagger.nouns(doc))
                words = [element for partition in word for element in partition if len(element) > 1]
            else:
                doc = ''.join(processed_lines)
                words = konlp_tagger.nouns(doc)
                words = [word for word in words if len(word)>1]

            output.append(words)
        end = time.time()
        print('Data Processing Computation Time :', end - start, ' seconds')
        return output

    def save(self, file, file_name='No_Title'):
        fi = open(self.save_path + file_name, 'wb')
        pickle.dump(file, fi)
        fi.close()


    def line_detection(self, lines):
        lin_num = len(lines)
        clean_lines = []
        line_index = 0

        hangul = re.compile('[^ ㄱ-ㅣ가-힣]+')
        while line_index < lin_num:
            if len(lines[line_index]) >= 15:
                candidate = hangul.sub('', lines[line_index])
                if len(candidate) > 10:
                    cleaned = [char for char in lines[line_index] if (char.isalpha() or char == ' ')]
                    clean_lines.append(''.join(cleaned))
            line_index += 1

        return clean_lines




class dict_builder(processor):
    def __init__(self):
        super(dict_builder, self).__init__()
        self.SPARQL_URL = "https://query.wikidata.org/sparql"
        self.doc_data = []

    def has_jongseong(self, word):
        # word라는 한글이 들어오면 끝에 종성이 있는지 없는지를 Boolean으로 나타내 줍니다.

        indicator = (ord(word[-1]) - 44032) % 28
        if indicator == 0:
            return False
        else:
            return True

    def execute_query(self, query):
        # Query가 들어오면 SPARQL에서 Query request하여 결과를 가져옵니다.
        sparql = SPARQLWrapper(self.SPARQL_URL)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        return pd.io.json.json_normalize(results['results']['bindings'])

    def google_req(self, q, unique = True):
        # 구글에서 q라는 단어를 검색하여 나오는 결과물 중 <em> 태그가 붙은 모든 결과를 불러옵니다. Using Request
        headers_Get = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-KR,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1'
        }
        s = requests.Session()
        q = '+'.join(q.split())
        url = 'https://www.google.com/search?q=' + q + '&ie=utf-8&oe=utf-8'
        r = s.get(url, headers=headers_Get)

        soup = BeautifulSoup(r.text, "html.parser")
        output = soup.findAll('em')
        output = [str(item).replace('<em>', '') for item in output]
        output = [item.replace('</em>', '') for item in output]
        if unique:
            return list(set(output))
        else:
            return output

    def google_sel(self, q, unique = True):
        # 구글에서 q라는 단어를 검색하여 나오는 결과물 중 <em> 태그가 붙은 모든 결과를 불러옵니다. Using Selenium


        driver = webdriver.Chrome()  # Optional argument, if not specified will search path.
        driver.get('http://www.google.com/xhtml');
        time.sleep(rd.uniform(low=0.5, high=1.5))  # Let the user actually see something!
        search_box = driver.find_element_by_name('q')
        search_box.send_keys(q)
        search_box.submit()
        html = driver.page_source
        time.sleep(rd.uniform(low=0.5, high=1.5))  # Let the user actually see something!
        driver.quit()



        soup = BeautifulSoup(html, "html.parser")
        output = soup.findAll('em')
        output = [str(item).replace('<em>', '') for item in output]
        output = [item.replace('</em>', '') for item in output]
        if unique:
            return list(set(output))
        else:
            return output

    def create_dictionary(self, dataframe, codec=0):
        # Query를 통해 받아온 Dataframe을 Mecab Dictionary 형태로 만들어 줍니다.
        dictionary = {'표층형': [],
                    '01': [],
                    '02': [],
                    '03': [],
                    '품사태그': [],
                    '의미부류': [],
                    '종성유무': [],
                    '읽기': [],
                    '타입': [],
                    '첫번째품사': [],
                    '마지막품사': [],
                    '원형': [],
                    '인덱스표현': []}
        dictionary = pd.DataFrame(dictionary)
        dictionary = dictionary[['표층형', '01', '02', '03', '품사태그', '의미부류', '종성유무', '읽기', '타입',
                                  '첫번째품사', '마지막품사', '원형', '인덱스표현']]

        # 한국 데이터가 없어 Q~~ 형식으로 Label이 되어있는 것들을 제거
        valid_ind = [ind for ind, inst in enumerate(dataframe['idLabel.xml:lang']) if isinstance(inst, str)]
        dat = [dataframe['idLabel.value'][ind].replace('(주)', "") for ind in valid_ind]
        dictionary['표층형'] = dat
        dictionary['읽기'] = dat
        dictionary['품사태그'] = ['NNP']*len(valid_ind)
        for i in range(1, 6):
            dictionary[dictionary.columns[-i]] = ['*']*len(valid_ind)
        dictionary[dictionary.columns[-8]] = ['*'] * len(valid_ind)

        jongsung = []
        for word in dat:
            jong_indicator = self.has_jongseong(word)
            if jong_indicator:
                jongsung.append('T')
            else:
                jongsung.append('F')
        dictionary['종성유무'] = jongsung

        if codec == 0:
            dictionary.to_csv('nnp.csv', sep=',', encoding='utf-8', header=False, index=False)
            print('Dictionary csv file created. The encoding if utf-8. You might not be able to read it on the windows excel')
        elif codec == 1:
            dictionary.to_csv('nnp.csv', sep=',', encoding='EUC-KR', header=False, index=False)
            print('Dictionary csv file created. The encoding EUC_KR. You may read this on windwos excel but do not use this csv file to compile.')

    def extract_NE(self, tagger='Hannanum'):
        LSA = LSA_ybl.LSA()
        if isinstance(self.data_path, list):
            LSA.data_path = self.data_path[0]
        else:
            LSA.data_path = self.data_path
        LSA.save_path = self.save_path
        if len(self.doc_data) == 0:
            LSA.raw_data = super(dict_builder, self).easy_process(tagger)
        else:
            LSA.raw_data = self.doc_data
        return LSA.extract_NE()

    def check_dict(self, word, tagger='Mecab'):
        if tagger == 'Mecab':
            konlp_tagger = konlpy.tag.Mecab()
        elif tagger == 'Kkma':
            konlp_tagger = konlpy.tag.Kkma()
        elif tagger == 'Twitter':
            konlp_tagger = konlpy.tag.Twitter()
        elif tagger == 'Hannanum':
            konlp_tagger = konlpy.tag.Hannanum()
        else:
            raise Exception(tagger, ' is not a tagger defined in konlpy')

        word_output = konlp_tagger.nouns(word)
        if len(word_output) != 0:
            if word == konlp_tagger.nouns(word)[0]:
                return True
            else:
                return False
        else:
            return False

    def read_data(self, file):
        LSA = LSA_ybl.LSA()
        return LSA.read_data(file)

    def Build_dict_txts(self, extract_tagger='Hannanum', dict_tagger='Mecab', google='sel'):

        NE = self.extract_NE(extract_tagger)
        output = []

        index_set = set([math.floor(i * len(NE) / 100) for i in range(100)])
        Total_index = 0
        for word in NE:
            if google == 'req':
                wset = set(self.google_req(word))
            else:
                wset = set(self.google_sel(word))

            if word in wset:  # Check if this word actually exists by looking at the google search result
                if self.check_dict(word, tagger=dict_tagger):  # Check if this word exist in the dictionary
                    pass
                else:
                    output.append(word)

            Total_index += 1
            if Total_index in index_set:
                print('Dictionary Candidate Searching ', math.floor((Total_index + 1)/len(NE)*100), '% Done')


        dataframe = {'idLabel.value': output, 'idLabel.xml:lang': ['ko']*len(output)}
        return dataframe




if __name__ == '__main__':
    db = dict_builder()
    query = """ SELECT ?id ?idLabel WHERE {
       ?id wdt:P31 wd:Q4830453.
       ?id wdt:P17 wd:Q884
          SERVICE wikibase:label {
       bd:serviceParam wikibase:language "ko" .
       }
    } """
    res = db.execute_query(query)
    db.create_dictionary(res)


    empres = db.google_req('하이화력')
    print(empres)

def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True