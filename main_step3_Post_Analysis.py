import LSA_Analysis

analytic_tool = LSA_Analysis.LSA_Analysis()
analytic_tool.NE = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/NE.pkl')
analytic_tool.TFIDF = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/TFIDF.pkl')
analytic_tool.OM = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/OM.pkl')
analytic_tool.TF = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/TF.pkl')
analytic_tool.IDF = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/IDF.pkl')
connection_str, correlation = analytic_tool.find_connections(['석탄', '에쓰오일', '후성', '만도'], '30')
# Drawing ------------------------------------------------------------------------
analytic_tool.draw_chart_basic(word = '석탄', font_path= 'C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Resource/Fonts/', save = 0)
analytic_tool.draw_chart_stats(word = '석탄', font_path= 'C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Resource/Fonts/', save = 0)
# analytic_tool.draw_chart_stats('대한항공', save = 1)
# analytic_tool.draw_chart_stats('매출액', save = 1)
# analytic_tool.draw_chart_basic('대한항공', list = ['CM', 'TF', 'IDF', 'Corr'], save = 1)
# analytic_tool.draw_chart_basic('매출액', list = ['CM', 'TF', 'IDF', 'Corr'], save = 1)
# analytic_tool.draw_chart_basic('호텔신라', list = ['CM', 'TF', 'IDF', 'Corr'], save = 1)

# Finding Connections -------------------------------------------------------------
# connection_str, correlation = analytic_tool.find_connections(['대한항공', '호텔신라', '면세점'], threshold='50') # If threshold is str, it returns the top threshold number of connections. If number, it returns the connections that is greater than the threshold.
# connection_multiple_str, multiple_correlation = analytic_tool.find_connections(['증권투자', '키움증권', '자료'], threshold='5')
# _, _ = analytic_tool.find_connections(1, threshold = '5')
# _, _ = analytic_tool.find_connections([1, 2, 3], threshold='5')


