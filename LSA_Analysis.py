from LSA_Model import LSA
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
matplotlib.rc('font', family='Arial')

class LSA_Analysis(LSA):
    def draw_chart_basic(self, word, font_path, save=0):
        print("Drawing Chart OM, TF, IDF")
        path = font_path + '아리따-돋움(TTF)-Bold.ttf'
        fontprop = fm.FontProperties(fname=path, size=12)
        fontprop2 = fm.FontProperties(fname=path, size=8)
        if isinstance(word, str):
            ind = self.find_NE_ind(word)
        elif isinstance(word, int):
            ind = word
        else:
            raise Exception('Input word has to be a string name or integer index')

        Docs_axis = np.array(range(self.OM.shape[1]))
        chart_ind = 1
        fig = plt.figure()


        plt.subplot(220 + chart_ind)
        plt.title(u'Count Matrix (' + str(word) + ')', fontproperties=fontprop)
        plt.bar(Docs_axis, self.OM.toarray()[ind, :])

        chart_ind += 1


        plt.subplot(220 + chart_ind)
        plt.bar(Docs_axis, np.squeeze(np.asarray(self.TF[ind, :])))
        plt.title('Term Frequency(' + str(word) + ')', fontproperties=fontprop, y=1)
        chart_ind += 1


        plt.subplot(220 + chart_ind)
        y = np.squeeze(self.IDF)
        sorted_index = np.argsort(y)[::-1]
        index = np.where(sorted_index == ind)[0][0]
        sorted_y = np.sort(y)[::-1]
        plt.bar(np.array(range(len(self.NE))), sorted_y, 0.5, )
        plt.plot(np.array(range(len(self.NE))) , self.IDF[ind]*np.ones(len(self.NE)), 'r')
        if len(str(self.IDF[ind])) > 5:
            idfval = str(self.IDF[ind])[1:6]
        else:
            idfval = str(self.IDF[ind])
        plt.title('IDF(' + str(word) + ') '+ idfval, fontproperties=fontprop, y=1)
        print('IDF value is:', self.IDF[ind])
        chart_ind += 1


        plt.subplot(220 + chart_ind)
        connection_str, correlation = self.find_connections(word, threshold='20')
        correlation[0] = 0
        plt.bar(np.array(range(len(correlation))), correlation)
        plt.title('Top 20 Corr(' + str(word) + ')', fontproperties=fontprop, y=1)
        plt.xticks(np.arange(len(correlation)), connection_str, fontproperties=fontprop2, rotation='vertical')
        chart_ind += 1

        if save == 0:
            plt.show()
        elif save == 1:
            plt.savefig('Figure_' + word + '.png', dpi=1000)
            print('Figure saved')


    def draw_chart_stats(self, word, font_path, save = 0):
        print("Drawing Chart Stats")
        path = font_path + '아리따-돋움(TTF)-Light.ttf'
        font_dirs = path
        font_files = fm.findSystemFonts()
        font_list = fm.createFontList(font_files)
        fm.fontManager.ttflist.extend(font_list)
        font_name = fm.FontProperties(fname=path, size=50).get_name()

        fontprop = fm.FontProperties(fname=path, size=11)
        fontprop2 = fm.FontProperties(fname=path, size=7)
        font1 = {'family': font_name, 'color': 'black', 'size': 11, 'weight': 'bold'}
        font2 = {'family': font_name, 'color': 'black', 'size': 7, 'weight': 'bold'}

        if isinstance(word, str):
            ind = self.find_NE_ind(word)
        elif isinstance(word, int):
            ind = word
        else:
            raise Exception('Input word has to be a string name or integer index')
        word_cut = 20

        connection_str, correlation = self.find_connections(word, threshold=str(word_cut))
        name_ind = []
        for i in range(len(connection_str)):
            name_ind.append(self.find_NE_ind(connection_str[i]))
        word_axis = np.array(range(word_cut))
        chart_num = 4
        chart_ind = 1

        f, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)

        # Top Corr sorted angles
        row_normalized = self.OM.toarray()/np.linalg.norm(self.OM.toarray(), axis=1, keepdims=True)
        test = np.linalg.norm(row_normalized, axis = 1)
        raw_angle = np.matmul(row_normalized, row_normalized[ind,:])
        raw_angle = np.arccos(raw_angle)*180/np.pi
        top_angle = [raw_angle[nind] for nind in name_ind]
        ax1.plot(word_axis, top_angle)
        ax1.set_title('Corr Sorted Angles to (' + str(word) + ')', fontdict=font1)
        ax1.set_xticks(word_axis)
        ax1.set_xticklabels(connection_str , fontdict=font2)
        ax1.tick_params(axis='x', rotation=90, length=2, width=2)
        chart_ind += 1

        # Top angles
        angl_ind = np.argsort(raw_angle)
        angl_ind = angl_ind[:20]
        top20_angl = raw_angle[angl_ind]
        top20_name = self.NE[angl_ind]

        ax2.plot(word_axis, top20_angl)
        ax2.set_title('Bottom 20 Angles to (' + str(word) + ')', fontdict=font1)
        ax2.set_xticks(word_axis)
        ax2.set_xticklabels(top20_name, fontdict=font2)
        ax2.tick_params(axis='x', rotation=90, length=2, width=2)
        chart_ind += 1


        # TF Sum and IDF
        TF_SUM = []
        idfs = []
        for ind in name_ind:
            TF_SUM.append(np.sum(self.TF[ind, :]))
            idfs.append(self.IDF[ind])

        # ax3.plot(word_axis, TF_Var, '-o', color='tab:red')
        ax3.plot(word_axis, TF_SUM, color='tab:red')
        ax3.set_title('TF-SUM and IDF(' + str(word) + ')', fontdict=font1)
        ax3.set_xticks(word_axis)
        ax3.set_xticklabels(connection_str , fontdict=font2)
        ax3.set_ylabel('TF_SUM')
        ax3.tick_params(axis='x', rotation=90)
        ax3.tick_params(axis='y', labelcolor='tab:red', length=2, width=2)
        ax3.yaxis.get_offset_text().set_visible(False)

        ax3dup = ax3.twinx()
        ax3dup.set_ylabel('IDF')  # we already handled the x-label with ax1
        ax3dup.plot(word_axis, idfs)
        chart_ind += 1
        # ax3dup.tick_params(axis='y', labelcolor=color)





        # Correlation
        ax4.bar(np.array(range(len(correlation))), correlation)
        ax4.set_title('Top 20 Corr(' + str(word) + ')', fontdict =font1)
        ax4.set_xticks(word_axis)
        ax4.set_xticklabels(top20_name, fontdict=font2)
        ax4.tick_params(axis='x', rotation=90, length=2, width=2)
        # plt.subplot(100 * (chart_num//2) + 20 + chart_ind)
        # # correlation[0] = 0
        # plt.bar(np.array(range(len(correlation))), correlation)
        # plt.title('Top 20 Corr(' + str(word) + ')', fontproperties=fontprop, y=0.8)
        # plt.xticks(np.arange(len(correlation)), connection_str, fontproperties=fontprop2, rotation='vertical')
        chart_ind += 1

        f.tight_layout()  # otherwise the right y-label is slightly clipped
        if save == 0:
            plt.show()
        elif save == 1:
            plt.savefig('Fig_Stat_' + word + '.png', dpi=1000)
            print('Figure saved')