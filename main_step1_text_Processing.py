import textprocessor
import time
import os

# #Text Processing Step ------------------------------------------------
# Initialization step --------------------------------------------------
tp = textprocessor.processor()
tp.language = 'kr'
# tp.data_path = ["Raw_Data/fn_guide/" + folder + '/' for folder in os.listdir("Raw_Data/fn_guide/")] # -----Old Data
tp.data_path = ['C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Raw_Data/txt/']
tp.save_path = 'C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Processed_Data/'

# File List ------------------------------------------------------------
retrieval_condition = ['[ind for ind, inst in enumerate(df["stockcode"]) if not (isinstance(inst, float) and math.isnan(inst))]',
                       '[ind for ind, inst in enumerate(df["cat2"]) if inst == "에너지"]',
                       '[ind for ind, inst in enumerate(df["stockname"]) if inst == "OCI"]']
# retrieval_condition = ['[ind for ind, inst in enumerate(df["stockcode"]) if not (isinstance(inst, float) and math.isnan(inst))]',
#                        '[ind for ind, inst in enumerate(df["cat2"]) if inst == "소재"]']
# retrieval_condition = ['[ind for ind, inst in enumerate(df["stockcode"]) if not (isinstance(inst, float) and math.isnan(inst))]']
tp.file_list(csv_file='metaDataUnique.csv', condition_list=retrieval_condition)

# Input data English to Korean Translation Step------------------------
# tp.txt_english_translator('Resource/translation_dictionary_en2kr')
# tp.txt_non_korean_remover()

# Processor Step
start = time.time()
processed = tp.easy_process(tagger='Kkma')
tp.save(processed, 'example1.pkl')
end = time.time()
print(end-start)

