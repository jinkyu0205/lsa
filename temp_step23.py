import LSA_ybl

# #LSA Calculation of Occurence, Correlation Matrices
# Initialization Step (These are already set as default but for the clarification stated here)
model = LSA_ybl.LSA()
model.language = 'kr'
model.data_path = 'C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Processed_Data/'
model.save_path = 'C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/'
model.distinguish_folder = False

model.idf_mult = 2
model.read_raw_data('example1.pkl')  # This reads data and save it in self.raw_data
model.calc_reduced_TFIDF(OM_reduction_threshold=10, svd_reduc_num=0, method=0)  # This calculates correlation matrix. If TF-IDF, OM, NE not computed it does them first.

model.save(model.NE, file_name='NE.pkl')
model.save(model.OM, file_name='OM.pkl')
model.save(model.TFIDF, file_name='TFIDF.pkl')
model.save(model.TF, file_name='TF.pkl')
model.save(model.IDF, file_name = 'IDF.pkl')

# model.save_as_csv(model.OM, 'OM')
# model.save_as_csv(model.IDF, 'IDF')
# model.save_as_csv(model.TF, 'TF')
# model.save_as_csv(model.corr, 'Correlation')


analytic_tool = LSA_ybl.LSA_Analysis()
analytic_tool.NE = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/NE.pkl')
analytic_tool.TFIDF = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/TFIDF.pkl')
analytic_tool.OM = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/OM.pkl')
analytic_tool.TF = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/TF.pkl')
analytic_tool.IDF = analytic_tool.read_data('C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Trained_Data/IDF.pkl')
connection_str, correlation = analytic_tool.find_connections(['석탄', '에쓰오일', '후성', '만도'], '30')
# Drawing ------------------------------------------------------------------------
analytic_tool.draw_chart_basic(word = '석탄', font_path= 'C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Resource/Fonts/', save = 0)
analytic_tool.draw_chart_stats(word = '석탄', font_path= 'C:/Users/Jinkyu_Lawrence_Lee/Google 드라이브/Ontology GIYS/LSA_OntGen/Resource/Fonts/', save = 0)
# analytic_tool.draw_chart_stats('대한항공', save = 1)
# analytic_tool.draw_chart_stats('매출액', save = 1)
# analytic_tool.draw_chart_basic('대한항공', list = ['CM', 'TF', 'IDF', 'Corr'], save = 1)
# analytic_tool.draw_chart_basic('매출액', list = ['CM', 'TF', 'IDF', 'Corr'], save = 1)
# analytic_tool.draw_chart_basic('호텔신라', list = ['CM', 'TF', 'IDF', 'Corr'], save = 1)



