# LSA Model for the Ontology Extraction
## LSA_Model
This is the main LSA engine that contains various functions required to perform Latent Semantic Analysis on documents
## LSA_Analysis
This module allows you to analyze the results from LSA model above. This includes visual representations.
## main_step1_text_Processing
This code uses LSA_Model module to first convert the raw txt files into LSA-analyzable group of nouns.
## main_step2_LSA
This code uses LSA_Model to perform LSA
## main_step3_Post Analysis
This code uses LSA_Analysis module to conduct analysis on the results from LSA.